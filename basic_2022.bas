BORDER 0: PAPER 0: INK 7
cls

dim er, e1X, e1Y, e1C, e1E, e2X, e2Y, e2C, e2E, e3X, e3Y, e3C, e3E, fr, cX, cY, j, jC, life, eat, pB, pA, pAY, pC as Ubyte

#ifdef __CARGA_B
dim sSq, clY, clX, clLX, clLY as Ubyte
let sSq = 13
#endif 

dim sq1, sq2, sq3, sq4, sq5, sq6, sq7, sq8, sq9, sq10, sq11, sq12, sq13, sq14 as Ubyte
dim e1H, e1V, e2H, e2V, e3H, e3V, pE, jE, pH as Byte
dim t, t1, tM, tMI, tj, teat as float
dim sc, hSC as UInteger

Dim u2 (160) As uByte => { _
	0,255,128,128,129,157,191,191, _
	0,255,1,193,1,121,253,253, _
	191,191,191,191,159,143,128,255, _
	253,237,205,157,185,241,1,255, _
	0,255,128,143,145,169,165,162, _
	0,255,1,241,137,149,165,69, _
	189,162,165,169,145,143,128,255, _
	189,69,165,149,137,241,1,255, _	
	0,255,128,172,158,191,191,159, _
	0,255,1,1,1,113,249,253, _
	143,135,135,131,131,129,128,255, _
	237,221,189,249,241,241,1,255, _
	0,255,128,170,156,185,147,135, _
	0,255,1,1,225,241,185,109, _
	142,141,135,130,131,129,128,255, _
	221,181,109,221,189,249,1,255, _
	0,255,128,179,131,187,184,187, _
	0,255,1,129,129,177,49,1, _
	131,152,155,131,128,128,128,255, _
	97,109,13,97,109,13,1,255 _
}

Dim uM (160) As uByte => { _
	0,12,50,33,64,128,136,112, _
	1,1,126,128,0,0,0,0, _
	192,32,16,72,40,48,16,16, _
	16,32,32,32,32,16,16,8, _
	0,96,96,15,139,143,7,128, _
	208,208,16,208,72,200,136,8, _
	4,2,1,2,4,4,28,96, _
	0,255,1,1,2,2,1,0, _
	112,128,92,36,36,88,128,110, _
	160,144,144,72,69,34,34,30, _
	16,36,1,128,96,24,7,0, _
	25,17,18,146,34,36,228,24, _
	0,7,8,16,32,64,36,32, _
	0,192,48,16,8,40,4,4, _
	36,19,16,16,8,4,3,0, _
	38,200,8,16,224,128,0,0, _
	1,6,8,17,16,35,36,106, _
	192,48,8,4,2,18,194,34, _
	81,144,112,44,99,56,7,0, _
	50,210,26,36,228,24,240,0 _
}

Dim u (160) As uByte => { _
	0,0,0,3,3,0,0,0, _
	0,0,0,128,128,0,0,0, _
	0,31,48,97,76,72,64,0, _
	0,240,24,140,4,4,4,4, _
	64,96,32,48,127,103,0,0, _
	0,4,4,12,252,236,0,0, _
	0,15,24,49,32,32,32,32, _
	0,248,12,134,50,18,2,0, _
	0,32,32,48,63,55,0,0, _
	2,6,4,12,254,230,0,0, _
	0,119,111,31,49,51,51,127, _
	0,238,246,248,140,156,156,254, _
	120,119,117,55,56,31,48,0, _
	30,238,174,236,28,248,12,0, _	
	0,255,128,128,128,129,130,132, _
	0,255,1,65,193,65,33,17, _
	136,156,174,174,190,156,128,255, _
	17,57,93,93,125,57,1,255, _
	0,119,111,31,49,57,57,127, _
	0,238,246,248,140,204,204,254 _
}
Poke uInteger 23675, @u (1)

RANDOMIZE 0

let fr=0 : LET c1$= "\{i3}\K\L" : LET c2$ = "\{i3}\M\N"

let er=0 : LET e1$ = "\{i5}\C\D" : LET e2$ = "\{i5}\E\F" : LET pE = 10 : LET jE = 9 : let j = 0 : let jC = 3 : let life = 3 : LET c$ = "\{i7}\A\B" : LET p$ = "\{i3}\A\B"

#ifdef __CARGA_B
LET tp$ = "\{i1}\A\B"
#endif

let hSC = 0 : let sc = 0

DIM sG(11,7) as Ubyte => {_
	{ 1, 1, 1, 1, 1, 1, 1 },_
	{ 1, 0, 1, 0, 1, 0, 1 },_
	{ 1, 1, 1, 0, 1, 1, 1 },_
	{ 1, 0, 1, 1, 1, 0, 1 },_
	{ 1, 1, 1, 0, 1, 1, 1 },_
	{ 1, 0, 1, 1, 1, 0, 1 },_
	{ 1, 1, 1, 0, 1, 1, 1 },_
	{ 1, 0, 1, 1, 1, 0, 1 },_
	{ 1, 1, 1, 0, 1, 1, 1 },_
	{ 1, 0, 1, 0, 1, 0, 1 },_
	{ 1, 1, 1, 2, 1, 1, 1 }_
}


initLoad:
cls
PRINT at 0,5;"\{p1}\{i0}\:'\''\':\: \ '\{p0}\::\{p1}\:'\{i1}\::\{i0}\':\{i1}\::\{p0}\{i0}\::\{p1}\{i1}\::\::\::\{i0}\':\: \{i1}\::\{i0}\ '\{p0}\:.  \{p1}\{i1}\::\{p0}\''\{p1}\::\{i0}\':"; _
at 1,5;"\{p1}\{i0}\' \{i1}\::\::\{p0}\ :\{p1}\::\{i0}\ :\: \{i1}\::\{i0}\ :\{i1}\::\{p0}\{i0}\::\{p1}\{i1}\::\{p6}\{i0}\:'\{p1}\: \ :\: \ :\. \':\{p0}  \{p1}\{i1}\::\{p0}\{i6}\ :\{i1}\ :\: "; _
at 2,4;"\{i1}\ :\''\''\{p1}\::\{p0}\ :\{p1}\::\::\::\{p0}\':\: \{p1}\::\{p0}\{i0}\::\{p1}\{i1}\::\{p0}\{i6}\ :\{i1}\ :\: \ :\{i6}\ .\. \{p1}\{i1}\::\{i0}\':\{p0}\::\{p1}\{i1}\::\{i0}\:'\{i1}\::\{i0}\.:"; _
at 3,3;"\{p5}\{i0}\:'\{i1}\''\{p0}\{i6}\ :\: \{p5}\{i1}\''\{p0}\ '\' \{p5}\''\''\{p0}\ '\' \{p5}\''\{p0}\{i0}\::\{p5}\{i1}\''\{p0}\{i6}\ :\{i1}\ '\' \ '\{i6}\ '\' \{i1}\ '\{i5}\..\{i0}\::\{p5}\{i1}\''\{p0}\ '\' "; _ 
at 4,3;"\{i5}\.:\'   \{p5}\{i5}\::\{p0}\.:\: \ '\' \ :\:.\{p5}\::\{i0}\:'\ .\''\' \.:\' \.:\{p0} \{i0}\':\{p5}\. \{i5}\::\{p0}\:'\ '\{p5}\::\::\::\{i0}\ :\:.\..\..\.:"; _
at 13,8; "\{p0}\{i7}Adaptacion  de  la"; at 14,8; "tabletop de Gakken"; at 17,8; "Para  el concurso"; at 18,8; "BYTEMANIACOS 2022"; at 21,9; "Pulsa una tecla"; at 22,10; "para  empezar";

gosub printCerdoMarker
#ifdef __CARGA_A
print at 7,13; "CARA A";
#else
print at 7,13; "CARA B";
#endif

let a$ = "Controles: Q-arriba A-abajo O-izquierda P-derecha Sp o M-salto "
carrusel:

for x=1 TO len a$
 print at 9,7; a$( to 20);
 let a$ = a$ (2 to ) + a$ (1)
 pause 10
 if inkey$ <> "" then goto showScreen
next x

GOTO carrusel

showScreen:
let t1 = int((65536* peek 23674+256*peek 23673 + peek 23672)/10)
let tMI = int((65536* peek 23674+256*peek 23673 + peek 23672)/10)
let pE = pE - 2
if pE <= 0 then let pE = 1
let jE = jE - 1
if jE <= 5 then let jE = 5
let sq1 = 0: let sq2 = 0: let sq3 = 0: let sq4 = 0
let sq5 = 0: let sq6 = 0: let sq7 = 0: let sq8 = 0
let sq9 = 0: let sq10 = 0: let sq11 = 0: let sq12 = 0
let sq13 = 0: let sq14 = 0

gosub resetEne:

cls
for x=1 TO 11
	for y=1 TO 7
		if sG(x,y) > 0 then 
			print at x+(x-1),(y*2)+7;c$
		end if
	next y
next x

gosub printMarker
gosub printScTitle
gosub printSc
gosub printHighScore
gosub printCerdo
gosub printE

mBucle:

let t = int((65536* peek 23674+256*peek 23673 + peek 23672)/10)
if t >= t1 + pE then 
	GOSUB deleteEne

	if e1E = 0 then 
		if e1X >= 11  then
			let e1V = -1 
		else if e1V = -1 and e1X <= 1 then
			let e1V = 1
		end if

		if e1Y < 7 and e1H = 1 and sG(e1X,e1Y+1) > 0 and e1C <= 2 then
			let e1Y = e1Y + e1H : let e1C = e1C + 1
		else if e1Y > 1 and e1H = -1 and sG(e1X,e1Y-1) > 0 and e1C <= 2 then
			let e1Y = e1Y + e1H : let e1C = e1C + 1
		else
			if e1Y >= 6 then let e1H = -1
			if e1Y <= 1 then let e1H = 1
			let e1X = e1X + e1V : let e1C = 1
		end if
	end if

	if e2E = 0 then
		if e2X >= 11  then
			let e2V = -1 
		else if e2V = -1 and e2X <= 1 then
			let e2V = 1
		end if

		if e2Y < 7 and e2H = 1 and sG(e2X,e2Y+1) > 0 and e2C <= 2 then
			let e2Y = e2Y + e2H : let e2C = e2C + 1
		else if e2Y > 1 and e2H = -1 and sG(e2X,e2Y-1) > 0 and e2C <= 2 then
			let e2Y = e2Y + e2H : let e2C = e2C + 1
		else
			if e2Y >= 6 then let e2H = -1
			if e2Y <= 1 then let e2H = 1
			let e2X = e2X + e2V : let e2C = 1
		end if
	end if

	if e3E = 0 then
		if e3X >= 11  then
			let e3V = -1 
		else if e3V = -1 and e3X <= 1 then
			let e3V = 1
		end if

		if e3Y < 7 and e3H = 1 and sG(e3X,e3Y+1) > 0 and e3C <= 2 then
			let e3Y = e3Y + e3H : let e3C = e3C + 1
		else if e3Y > 1 and e3H = -1 and sG(e3X,e3Y-1) > 0 and e3C <= 2 then
			let e3Y = e3Y + e3H : let e3C = e3C + 1
		else
			if e3Y >= 6 then let e3H = -1
			if e3Y <= 1 then let e3H = 1
			let e3X = e3X + e3V : let e3C = 1
		end if
	end if

	GOSUB checkEne
	let t1 = int((65536* peek 23674+256*peek 23673 + peek 23672)/10)
end if

let tM = (65536* peek 23674+256*peek 23673 + peek 23672)/10
if j = 1 and tM >= tj + jE then let j = 0
if eat = 1 and tM >= teat + 20 then let eat = 0 : let e1E = 0 : let e2E = 0 : let e3E = 0 

if tM >= tMI + 1 then 	

	if eat = 1 then BEEP 0.02,35

	IF j = 1 then 
		BEEP 0.02,35
		BEEP 0.03,15
		BEEP 0.02,5
		BEEP 0.02,-5
		BEEP 0.01,45
		BEEP 0.02,35
	end IF

	let tMI = (65536* peek 23674+256*peek 23673 + peek 23672)/10

	rem salta los enemigos al pulsar spc (254, 190) o M (251,187)
	IF (In 32766 = 254 Or In 32766 = 190 OR In 32766 = 251 Or In 32766 = 187) and j = 0 and jC > 0 THEN
		let jC = jC - 1 : let j = 1 : let tj = (65536* peek 23674+256*peek 23673 + peek 23672)/10
		gosub printMarker
		GOSUB deleteEne	
	END If

	rem mueve a la derecha al pulsar p
	IF IN 57342=254 OR IN 57342=190 THEN 
		if cY < 7 and sG(cX,cY+1) > 0 then
			GOSUB deletePig
			#ifdef __CARGA_B
			gosub updatePos
			#endif
			let cY = cY + 1
			#ifdef __CARGA_A
			GOSUB checkSquare
			#else
			GOSUB checkB
			#endif
			gosub checkC
		end IF
	end IF

	rem mueve a la izquierda al pulsar o
	IF IN 57342=253 OR IN 57342=189 THEN 
		if cY > 1 and sG(cX,cY-1) > 0 then	
			GOSUB deletePig
			#ifdef __CARGA_B
			gosub updatePos
			#endif
			let cY = cY - 1
			#ifdef __CARGA_A
			GOSUB checkSquare
			#else
			GOSUB checkB
			#endif
			gosub checkC
		end IF
	end IF

	rem mueve arriba al pulsar q
	IF IN 64510=254 OR IN 64510=190 THEN
		if cX > 1 and sG(cX-1,cY) > 0 then
			GOSUB deletePig
			#ifdef __CARGA_B
			gosub updatePos
			#endif
			let cX = cX - 1
			#ifdef __CARGA_A
			GOSUB checkSquare
			#else
			GOSUB checkB
			#endif
			gosub checkC
		end IF
	end IF

	rem mueve abajo al pulsar a
	IF IN 65022=254 OR IN 65022=190 THEN
		if cX < 11 and sG(cX+1,cY) > 0 then
			GOSUB deletePig
			#ifdef __CARGA_B
			gosub updatePos
			#endif
			let cX = cX + 1
			#ifdef __CARGA_A
			GOSUB checkSquare
			#else
			GOSUB checkB
			#endif
			gosub checkC
		end IF
	end IF

end IF

go to mBucle    

#ifdef __CARGA_B
updatePos:
	let clLY = clY
	let clY = cY
	let clLX = clX
	let clX = cX
return 
#endif


deletePig:
	#ifdef __CARGA_A
	BEEP 0.05,15
	print at cX+(cX-1),(cY*2)+7;p$; at cX+(cX-1)+1,(cY*2)+7;"  "
	#else
	if (sG(cX, cY) = 3) then
		print at cX+(cX-1),(cY*2)+7;tp$
	else if (sG(cX, cY) = 2) then
		print at cX+(cX-1),(cY*2)+7;p$
	else	
		print at cX+(cX-1),(cY*2)+7;c$
	end if
	print at cX+(cX-1)+1,(cY*2)+7;"  "
	BEEP 0.08,-5
	#endif

return

deleteEne:
	if j = 0 and eat = 0 then BEEP 0.05,35
	if sG(e1X,e1Y) > 2 then 
		print at e1X+(e1X-1),(e1Y*2)+7;tp$
	else if sG(e1X,e1Y) > 1 then 
		print at e1X+(e1X-1),(e1Y*2)+7;p$
	else
		print at e1X+(e1X-1),(e1Y*2)+7;c$
	end if
	print at e1X+(e1X-1)+1,(e1Y*2)+7;"  "


	if sG(e2X,e2Y) > 2 then 
		print at e2X+(e2X-1),(e2Y*2)+7;tp$
	else if sG(e2X,e2Y) > 1 then 
		print at e2X+(e2X-1),(e2Y*2)+7;p$
	else
		print at e2X+(e2X-1),(e2Y*2)+7;c$
	end if
	print at e2X+(e2X-1)+1,(e2Y*2)+7;"  "

	if sG(e3X,e3Y) > 2 then 
		print at e3X+(e3X-1),(e3Y*2)+7;tp$
	else if sG(e3X,e3Y) > 1 then 
		print at e3X+(e3X-1),(e3Y*2)+7;p$
	else
		print at e3X+(e3X-1),(e3Y*2)+7;c$
	end if
	print at e3X+(e3X-1)+1,(e3Y*2)+7;"  "
return

checkEne:

	if eat = 1 and ((e1X = cX and e1Y = cY) or _
		(e2X = cX and e2Y = cY) or _
		(e3X = cX and e3Y = cY)) then
		gosub eatEne
	end if

	if j = 0 and eat = 0 and ((e1X = cX and e1Y = cY) or _
		(e2X = cX and e2Y = cY) or _
		(e3X = cX and e3Y = cY)) then
		go sub colision
		GOTO mBucle
	end if

	let er=1-er
	gosub altE

	if j = 0 then gosub printE
return

checkC:

	if j = 0 and eat = 0 and ((e1X = cX and e1Y = cY) or _
		(e2X = cX and e2Y = cY) or _
		(e3X = cX and e3Y = cY)) then
		go sub colision
		GOTO mBucle
	end if

	if eat = 1 and ((e1X = cX and e1Y = cY) or _
		(e2X = cX and e2Y = cY) or _
		(e3X = cX and e3Y = cY)) then
		go sub eatEne
	end if

	gosub alternarCerdo	
	gosub printCerdo
	

	#ifdef __CARGA_B
	if sG(cX,cY) = 3 then 
		let sc = sc + 10
		gosub printSc
	end if
	#endif

return

#ifdef __CARGA_B
closeR:
	let sSq = -1
	for x=1 TO 11
		for y=1 TO 7
			if sG(x,y) = 3 then 
				let sG(x,y) = 2
				print at x+(x-1),(y*2)+7;p$
			end if
		next y
	next x
	if j = 0 then gosub printE
return

resetRect:
	let sSq = -1
	for x=1 TO 11
		for y=1 TO 7
			if sG(x,y) = 3 then 
				let sG(x,y) = 1
				print at x+(x-1),(y*2)+7;c$
			end if
		next y
	next x
return

#endif

checkB:
#ifdef __CARGA_B

	if cX = 11 and cY = 4 then
		let sSq = 13
	else if sG(cX,cY) = 3 or (sG(cX,cY) = 2 and cX = clLX and cY = clLY) then 
		if cX = clLX and cY = clLY then let clLX = 0 : let clLY = 0 
		gosub resetRect
		gosub checkB			
	else if sq13 <> 1 and sSq = 13 and _
		(((cX = 11 or cX = 10 or cX = 9 or cX = 8) and (cY = 3 or cY = 5)) or _
		((cX = 11 or cX = 8) and cY = 4)) then			
			let sSq = 13

			if (cX <> 11 or cY <> 4) and sG(cX,cY) = 1 then let sG(cX,cY) = 3

			if sG(11,3) > 1 and sG(11,5) > 1 and _ 
				sG(10,3) > 1 and sG(10,5) > 1 and _ 
				sG(9,3) > 1 and sG(9,5) > 1 and _
				sG(8,3) > 1 and sG(8,4) > 1 and sG(8,5) > 1 then

				let sq13 = 1
				Poke uInteger 23675, @u2 (1)
				print at 17,15;"\{i2}\A\B"; at 18,15;"\{i2}\C\D"
				Poke uInteger 23675, @u (1)
				print at 19,15;"\{i2}\O\P"; at 20,15;"\{i2}\Q\R"

				gosub closeR
				gosub checkW
				gosub checkB
			end if

	else if sq12 <> 1 and ( _ 
		(sq13 = 1 and ((cX = 9 or cX = 10 or cX = 11) and cY = 3)) or _
		(sq10 = 1 and (cX = 9 and (cY = 1 or cY = 2 or cY = 3))) or _
		(sSq = 12 and (((cX = 11 or cX = 9) and (cY = 1 or cY = 2)) or (cX = 10 and cY = 1)))) then

		let sSq = 12
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(11,1) > 1 and sG(11,2) > 1 and sG(11,3) > 1 and _ 
			sG(10,1) > 1 and sG(10,3) > 1 and _ 
			sG(9,1) > 1 and sG(9,2) > 1 and sG(9,3) > 1 then
			let sq12 = 1
			Poke uInteger 23675, @u2 (1)
			print at 19,11;"\{i6}\I\J"; at 20,11;"\{i6}\K\L"
			Poke uInteger 23675, @u (1)
			gosub closeR
			gosub checkEat
			gosub checkW
			gosub checkB
		end if

	else if sq14 <> 1 and (_
		(sq13 = 1 and ((cX =9 or cX = 10 or cX = 11) and cY = 5)) or _
		(sq11 = 1 and cX = 9 and (cY = 5 or cY = 6 or cY = 7)) or _
		(sSq = 14 and (((cX = 11 or cX = 9) and (cY = 7 or cY = 6)) or (cX = 10 and cY = 7)))) then

		let sSq = 14
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(11,5) > 1 and sG(11,6) > 1 and sG(11,7) > 1 and _ 
			sG(10,5) > 1 and sG(10,7) > 1 and _ 
			sG(9,5) > 1 and sG(9,6) > 1 and sG(9,7) > 1 then
			let sq14 = 1
			Poke uInteger 23675, @u2 (1)
			print at 19,19;"\{i6}\E\F"; at 20,19;"\{i6}\G\H"
			Poke uInteger 23675, @u (1)			
			gosub closeR
			gosub checkEat
			gosub checkW
			gosub checkB
		end if

	else if sq8 <> 1 and ( _
		(sq13 = 1 and cX = 8 and (cY = 3 or cY = 4 or cY = 5)) or _
		(sq10 = 1 and cX = 7 and cY = 3 and sG(5,3) <> 3) or _
		(sq11 = 1 and cX = 7 and cY = 5 and sG(5,6) <> 3) or _
		(sq7 = 1 and ((cX = 7 or cX = 6) and cY = 3)) or _
		(sq9 = 1 and ((cX = 7 or cX = 6) and cY = 5)) or _ 
		(sq5 = 1 and (cX = 6 and (cY = 3 or cY = 4 or cY = 5))) or _ 
		(sSq = 8 and (((cX = 6 or cX = 7 or cX = 8) and (cY = 3 or cY = 5)) or ((cX = 6 or cX = 8) and cY = 4))) or _
		(sSq = 7 and sq7 = 1 and cX = 7 and cY = 3) or _ 
		(((sSq = 10 and cY = 3) or (sSq = 11 and cY = 5)) and cX = 6 )) then
		
		let sSq = 8			
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(6,3) > 1 and sG(6,4) > 1 and sG(6,5) > 1 and _ 
			sG(7,3) > 1 and sG(7,5) > 1 and _ 
			sG(8,3) > 1 and sG(8,4) > 1 and sG(8,5) > 1 then
			let sq8 = 1
			Poke uInteger 23675, @u2 (1)
			print at 13,15;"\{i6}\I\J"; at 14,15;"\{i6}\K\L"
			Poke uInteger 23675, @u (1)			
			gosub closeR
			gosub checkW
			gosub checkB
		end if

	else if sq10 <> 1 and (_
		(sq13 = 1 and ((cX = 9 or cX = 8) and cY = 3)) or _
		(sq12 = 1 and cX = 9 and (cY = 1 or cY = 2 or cY = 3)) or _ 
		(sq7 = 1 and cX = 7 and (cY = 1 or cY = 2 or cY = 3)) or _ 
		(sq8 = 1 and cX = 7 and cY = 3) or _
		(sSq = 10 and (((cX = 7 or cX = 8 or cX = 9) and cY = 1) or _
					((cX = 7 or cX = 9) and cY = 2) or _
					(cX = 7 and cY = 3))) or _ 
		(((sSq = 12 and sG(9,2) <> 1) or (sSq = 7 and sG(7,2) <> 1)) and cX = 8 and cY = 1) or _
		(sSq = 8 and cX = 7 and cY = 2 and (sG(8,3) <> 1))) then
		
		let sSq = 10
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(9,1) > 1 and sG(9,2) > 1 and sG(9,3) > 1 and _ 
			sG(8,1) > 1 and sG(8,3) > 1 and _ 
			sG(7,1) > 1 and sG(7,2) > 1 and sG(7,3) > 1 then
			let sq10 = 1
			Poke uInteger 23675, @u2 (1)
			print at 15,11;"\{i4}\M\N"; at 16,11;"\{i4}\O\P"
			Poke uInteger 23675, @u (1)			
			gosub closeR
			gosub checkW
			gosub checkB
		end if

	else if sq11 <> 1 and (_
		(sq13 = 1 and sSq = -1 and ((cX = 9 or cX = 8) and cY = 5)) or _
		(sq14 = 1 and cX = 9 and (cY = 5 or cY = 6 or cY = 7)) or _
		(sq9 = 1 and cX = 7 and (cY = 5 or cY = 6 or cY = 7)) or _
		(sq8 = 1 and cX = 7 and cY = 5) or _
		(sSq = 11 and ((cX = 7 and cY = 5) or _
			((cX = 7 or cX = 9) and (cY = 6 or cY = 7)) or _
			((cX = 7 or cX = 8 or cX = 9) and cY = 7))) or _
		(((sSq = 14 and sG(9,6) <> 1) or (sSq = 9 and sG(7,6) <> 1)) and cX = 8 and cY = 7) or _
		(sSq = 8 and cX = 7 and cY = 6 and sG(8,5) <> 1)) then
		
		let sSq = 11			
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(9,5) > 1 and sG(9,6) > 1 and sG(9,7) > 1 and _ 
			sG(8,5) > 1 and sG(8,7) > 1 and _ 
			sG(7,5) > 1 and sG(7,6) > 1 and sG(7,7) > 1 then
			let sq11 = 1
			Poke uInteger 23675, @u2 (1)
			print at 15,19;"\{i5}\Q\R"; at 16,19;"\{i5}\S\T"
			Poke uInteger 23675, @u (1)			
			gosub closeR
			gosub checkW
			gosub checkB
		end if

	else if sq5 <> 1 and ((sq8 = 1 and (cX = 6 and (cY = 3 or cY = 4 or cY = 5))) or _		
		(((sq7 = 1 and cY = 3) or (sq9 = 1 and cY = 5)) and (cX = 5 or cX = 6) ) or _
		(((sq4 = 1 and cY = 3) or (sq6 = 1 and cY = 5)) and sSq = -1 and (cX = 4 or cX = 5) ) or _
		(sq2 = 1 and cX = 4 and (cY = 3 or cY = 4 or cY = 5)) or _
		(sSq = 5 and (((cX = 4 or cX = 5 or cX = 6) and (cY = 3 or cY = 5)) or _
			((cX = 4 or cX = 6) and cY = 4))) or _
		(sSq = 7 and cX = 4 and cY = 3 and (sG(6,4) = 2)) or _ 
		(sSq = 8 and cX = 5 and ((cY = 3 and sq9 = 1) or (cY = 5 and sq7 = 1)) and sG(6,4) <> 1)) then

		let sSq = 5
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(4,3) > 1 and sG(4,4) > 1 and sG(4,5) > 1 and _ 
			sG(5,3) > 1 and sG(5,5) > 1 and _ 
			sG(6,3) > 1 and sG(6,4) > 1 and sG(6,5) > 1 then			
			let sq5 = 1
			Poke uInteger 23675, @u2 (1)
			print at 9,15;"\{i6}\E\F"; at 10,15;"\{i6}\G\H"
			Poke uInteger 23675, @u (1)			
			gosub closeR
			gosub checkW
			gosub checkB
		end if

	else if sq7 <> 1 and ( _
		(sq8 = 1 and ((cX = 7 or cX = 6) and cY = 3)) or _
		(sq5 = 1 and ((cX = 5 or cX = 6) and cY = 3)) or _
		(sq10 = 1 and (cX = 7 and (cY = 1 or cY = 2 or cY = 3))) or _
		(sq4 = 1 and sSq = -1 and (cX = 5 and (cY = 1 or cY = 2 or cY = 3))) or _
		(sSq = 7 and (((cX = 5 or cX = 6 or cX = 7) and (cY = 1 or cY = 3)) or _
			((cX = 5 or cX = 7) and cY = 2))) or _ 
		(cX = 6 and cY = 1 and ((sSq = 10 and sG(7,3) = 2 and sG(7,2) <> 1) or (sSq = 4 and sG(5,2) = 2))) or _
		(sSq = 5 and cX = 5 and cY = 2 and sq5 = 1) or _
		(sSq = 8 and cX = 6 and cY = 3 and sG(7,3) = 2) or _
		(sSq = 8 and cX = 5 and cY = 3 and sG(7,2) = 2)) then

		let sSq = 7
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(5,1) > 1 and sG(5,2) > 1 and sG(5,3) > 1 and _ 
			sG(6,1) > 1 and sG(6,3) > 1 and _ 
			sG(7,1) > 1 and sG(7,2) > 1 and sG(7,3) > 1 then			
			let sq7 = 1
			print at 11,11;"\{i2}\O\P"; at 12,11;"\{i2}\Q\R"			
			gosub closeR
			gosub checkW
			gosub checkB
		end if

	else if sq9 <> 1 and ((sq8 = 1 and ((cX = 7 or cX = 6) and cY = 5)) or _
		(sq5 = 1 and ((cX = 5 or cX = 6) and cY = 5)) or _
		(sq11 = 1 and (cX = 7 and (cY = 5 or cY = 6 or cY = 7))) or _
		(sq6 = 1 and sSq = -1 and (cX = 5 and (cY = 5 or cY = 6 or cY = 7))) or _
		(sSq = 9 and (((cX = 5 or cX = 6 or cX = 7) and (cY = 5 or cY = 7)) or _
			((cX = 5 or cX = 7) and cY = 6))) or _ 
		(cX = 6 and cY = 7 and ((sSq = 11 and sG(7,5) = 2 and sG(7,6) <> 1) or (sSq = 6 and sG(5,6) = 2))) or _
		(sSq = 5 and cX = 5 and cY = 6 and sq5 = 1) or _
		(sSq = 5 and cX = 7 and cY = 5 and sG(5,6) = 2 and sG(6,5) <> 1) or _
		(sSq = 8 and ((cX = 6 and cY = 5 and sG(7,5) = 2) or (cX = 5 and cY = 5 and sG(7,6) = 2)))) then

		let sSq = 9
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(5,5) > 1 and sG(5,6) > 1 and sG(5,7) > 1 and _ 
			sG(6,5) > 1 and sG(6,7) > 1 and _ 
			sG(7,5) > 1 and sG(7,6) > 1 and sG(7,7) > 1 then			
			let sq9 = 1
			Poke uInteger 23675, @u2 (1)
			print at 11,19;"\{i2}\A\B"; at 12,19;"\{i2}\C\D"
			Poke uInteger 23675, @u (1)			
			gosub closeR
			gosub checkW
			gosub checkB
		end if


	else if sq4 <> 1 and ((sq5 = 1 and ((cX = 4 or cX = 5) and cY = 3)) or _
		(sq7 = 1 and (cX = 5 and (cY = 1 or cY = 2 or cY = 3))) or _
		(sq1 = 1 and (cX = 3 and (cY = 1 or cY = 2 or cY = 3))) or _
		(sq2 = 1 and ((cX = 3 or cX = 4) and cY = 3)) or _
		(sSq = 4 and (((cX = 3 or cX = 4 or cX = 5) and (cY = 1 or cY = 3)) or _
			((cX = 3 or cX = 5) and cY = 2))) or _ 
		(sSq = 5 and cX = 3 and cY = 3 and sG(4,4) = 1 and sG(5,2) = 2) or _
		(cX = 4 and cY = 1 and ((sSq = 7 and sG(4,4) = 2 and sG(5,2) <> 1) or (sSq = 1 and sG(3,2) = 2)))) then

		let sSq = 4
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(3,1) > 1 and sG(3,2) > 1 and sG(3,3) > 1 and _ 
			sG(4,1) > 1 and sG(4,3) > 1 and _ 
			sG(5,1) > 1 and sG(5,2) > 1 and sG(5,3) > 1 then			
			let sq4 = 1
			Poke uInteger 23675, @u2 (1)
			print at 7,11;"\{i5}\Q\R"; at 8,11;"\{i5}\S\T";
			Poke uInteger 23675, @u (1)			
			gosub closeR
			gosub checkW
			gosub checkB
		end if

	else if sq6 <> 1 and ((sq5 = 1 and ((cX = 4 or cX = 5) and cY = 5)) or _
		(sq9 = 1 and (cX = 5 and (cY = 5 or cY = 6 or cY = 7))) or _
		(sq3 = 1 and (cX = 3 and (cY = 5 or cY = 6 or cY = 7))) or _
		(sq2 = 1 and ((cX = 3 or cX = 4) and cY = 5)) or _
		(sSq = 6 and (((cX = 3 or cX = 4 or cX = 5) and (cY = 5 or cY = 7)) or _
			((cX = 3 or cX = 5) and cY = 6))) or _ 
		(sSq = 5 and cX = 3 and cY = 5 and sG(4,4) = 1 and sG(5,6) = 2) or _
		(cX = 4 and cY = 7 and ((sG(4,4) = 2 and sG(5,6) <> 1 and sSq = 9) or (sSq = 3 and sG(3,6) = 2)))) then

		let sSq = 6
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(3,5) > 1 and sG(3,6) > 1 and sG(3,7) > 1 and _ 
			sG(4,5) > 1 and sG(4,7) > 1 and _ 
			sG(5,5) > 1 and sG(5,6) > 1 and sG(5,7) > 1 then			
			let sq6 = 1
			Poke uInteger 23675, @u2 (1)
			print at 7,19;"\{i4}\M\N"; at 8,19;"\{i4}\O\P"
			Poke uInteger 23675, @u (1)			
			gosub closeR
			gosub checkW
			gosub checkB
		end if

	else if sq1 <> 1 and ((sq2 = 1 and ((cX = 1 or cX = 2 or cX = 3) and cY = 3)) or _
		(sq4 = 1 and (cX = 3 and (cY = 1 or cY = 2 or cY = 3))) or _
		(sSq = 1 and (((cX = 1 or cX = 2 or cX = 3) and (cY = 1 or cY = 3)) _
			or ((cX = 1 or cX = 3) and cY = 2))) or _
		(sSq = 4 and cX = 2 and cY = 1 and sG(3,3) = 2 and sG(3,2) <> 1))  then
		
		let sSq = 1
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(1,1) > 1 and sG(1,2) > 1 and sG(1,3) > 1 and _ 
			sG(2,1) > 1 and sG(2,3) > 1 and _
			sG(3,1) > 1 and sG(3,2) > 1 and sG(3,2) > 1 then			
			let sq1 = 1
			Poke uInteger 23675, @u2 (1)
			print at 3,11;"\{i6}\I\J"; at 4,11;"\{i6}\K\L";
			Poke uInteger 23675, @u (1)			
			gosub closeR
			gosub checkEat
			gosub checkW
			gosub checkB
		end if

	else if sq3 <> 1 and ((sq2 = 1 and ((cX = 1 or cX = 2 or cX = 3) and cY = 5)) or _
		(sq6 = 1 and (cX = 3 and (cY = 5 or cY = 6 or cY = 7))) or _
		(sSq = 3 and (((cX = 1 or cX = 2 or cX = 3) and (cY = 5 or cY = 7)) _ 
			or ((cX = 1 or cX = 3) and cY = 6))) or _
		(sSq = 6 and cX = 2 and cY = 7 and sG(3,5) = 2 and sG(3,6) <> 1))  then

		let sSq = 3
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(1,5) > 1 and sG(1,6) > 1 and sG(1,7) > 1 and _ 
			sG(2,5) > 1 and sG(2,7) > 1 and _
			sG(3,5) > 1 and sG(3,6) > 1 and sG(3,7) > 1 then			
			let sq3 = 1
			Poke uInteger 23675, @u2 (1)
			print at 3,19;"\{i6}\E\F"; at 4,19;"\{i6}\G\H";
			Poke uInteger 23675, @u (1)			
			gosub closeR
			gosub checkEat
			gosub checkW
			gosub checkB
		end if

	else if sq2 <> 1 and (((cX = 1 or cX = 2 or cX = 3 or cX = 4) and (cY = 3 or cY = 5)) or _
			((cX = 1 or cX = 4) and cY = 4)) and ((sG(cX,cY) = 2 or sSq = 2) or _
			 (cX = 1 and cY = 4 and (sG(4,4) = 2 or sG(3,3) = 2 or sG(3,5) = 2) and (sG(2,3) = 3 or sG(2,5) = 3)) or _
			 (cX = 3 and ((cY = 5 and sq4 = 1) or (cY = 3 and sq6 = 1)) and sG(4,4) <> 1 and sSq = 5) or _
			 (cX = 2 and ((cY = 5 and sSq = 6) or (cY = 3 and sSq = 4)) and sq5 = 1)) then

		let sSq = 2
		if sG(cX,cY) = 1 then let sG(cX,cY) = 3
		if sG(1,3) > 1 and sG(1,4) > 1 and sG(1,5) > 1 and _ 
			sG(2,3) > 1 and sG(2,5) > 1 and _ 
			sG(3,3) > 1 and sG(3,5) > 1 and _
			sG(4,3) > 1 and sG(4,4) > 1 and sG(4,5) > 1 then			
			let sq2 = 1
			print at 3,15;"\{i2}\O\P"; at 4,15;"\{i2}\Q\R";
			Poke uInteger 23675, @u2 (1)
			print at 5,15;"\{i2}\A\B"; at 6,15;"\{i2}\C\D";
			Poke uInteger 23675, @u (1)
			gosub closeR
			gosub checkW
			gosub checkB
		end if
	else
		gosub resetRect
	end if

#endif
return

checkSquare:

#ifdef __CARGA_A
	
	if sG(cX,cY) = 1 then let sG(cX,cY) = 2

	if (sq1 = 0 and ((cX = 3 and (cY = 1 or cY = 2 or cY = 3)) or _
		(cX = 2 and (cY = 1 or cY = 3)) or _
		(cX = 1 and (cY = 1 or cY = 2 or cY = 3)))) then
		if sG(3,1) > 1 and sG(3,2) > 1 and sG(3,3) > 1 and _ 
			sG(2,1) > 1 and sG(2,3) > 1 and _ 
			sG(1,1) > 1 and sG(1,2) > 1 and sG(1,3) > 1 then
			let sq1 = 1
			Poke uInteger 23675, @u2 (1)
			print at 3,11;"\{i6}\I\J"; at 4,11;"\{i6}\K\L";
			Poke uInteger 23675, @u (1)
			gosub checkEat
			gosub checkW
		end if
	end if

	if (sq2 = 0 and ((cX = 4 and (cY = 3 or cY = 4 or cY = 5)) or _
		(cX = 3 and (cY = 3 or cY = 5)) or _
		(cX = 2 and (cY = 3 or cY = 5)) or _
		(cX = 1 and (cY = 3 or cY = 4 or cY = 5)))) then
		if sG(4,3) = 2 and sG(4,4) > 1 and sG(4,5) > 1 and _ 
			sG(3,3) > 1 and sG(3,5) > 1 and _ 
			sG(2,3) > 1 and sG(2,5) = 2 and _
			sG(1,3) > 1 and sG(1,4) > 1 and sG(1,5) > 1 then
			let sq2 = 1
			print at 3,15;"\{i2}\O\P"; at 4,15;"\{i2}\Q\R";
			Poke uInteger 23675, @u2 (1)
			print at 5,15;"\{i2}\A\B"; at 6,15;"\{i2}\C\D";
			Poke uInteger 23675, @u (1)
			gosub checkW
		end if
	end if

	if (sq3 = 0 and ((cX = 3 and (cY = 5 or cY = 6 or cY = 7)) or _
		(cX = 2 and (cY = 5 or cY = 7)) or _
		(cX = 1 and (cY = 5 or cY = 6 or cY = 7)))) then
		if sG(3,5) > 1 and sG(3,6) > 1 and sG(3,7) > 1 and _ 
			sG(2,5) > 1 and sG(2,7) > 1 and _ 
			sG(1,5) > 1 and sG(1,6) > 1 and sG(1,7) > 1 then
			let sq3 = 1
			Poke uInteger 23675, @u2 (1)
			print at 3,19;"\{i6}\E\F"; at 4,19;"\{i6}\G\H";
			Poke uInteger 23675, @u (1)
			gosub checkEat
			gosub checkW
		end if
	end if

	if (sq4 = 0 and ((cX = 5 and (cY = 1 or cY = 2 or cY = 3)) or _
		(cX = 4 and (cY = 1 or cY = 3)) or _
		(cX = 3 and (cY = 1 or cY = 2 or cY = 3)))) then
		if sG(5,1) > 1 and sG(5,2) > 1 and sG(5,3) > 1 and _ 
			sG(4,1) > 1 and sG(4,3) > 1 and _ 
			sG(3,1) > 1 and sG(3,2) > 1 and sG(3,3) > 1 then
			let sq4 = 1
			Poke uInteger 23675, @u2 (1)
			print at 7,11;"\{i5}\Q\R"; at 8,11;"\{i5}\S\T";
			Poke uInteger 23675, @u (1)
			gosub checkW
		end if
	end if

	if sq5 = 0 and ((cX = 6 and (cY = 3 or cY = 4 or cY = 5)) or _
		(cX = 5 and (cY = 3 or cY = 5)) or _
		(cX = 4 and (cY = 3 or cY = 4 or cY = 5))) then
		if sG(6,3) > 1 and sG(6,4) > 1 and sG(6,5) > 1 and _ 
			sG(5,3) > 1 and sG(5,5) > 1 and _ 
			sG(4,3) > 1 and sG(4,4) > 1 and sG(4,5) > 1 then
			let sq5 = 1
			Poke uInteger 23675, @u2 (1)
			print at 9,15;"\{i6}\E\F"; at 10,15;"\{i6}\G\H"
			Poke uInteger 23675, @u (1)
			gosub checkW
		end if
	end if

	if (sq6 = 0 and ((cX = 5 and (cY = 5 or cY = 6 or cY = 7)) or _
		(cX = 4 and (cY = 5 or cY = 7)) or _
		(cX = 3 and (cY = 5 or cY = 6 or cY = 7)))) then
		if sG(5,5) > 1 and sG(5,6) > 1 and sG(5,7) > 1 and _ 
			sG(4,5) > 1 and sG(4,7) > 1 and _ 
			sG(3,5) > 1 and sG(3,6) > 1 and sG(3,7) > 1 then
			let sq6 = 1
			Poke uInteger 23675, @u2 (1)
			print at 7,19;"\{i4}\M\N"; at 8,19;"\{i4}\O\P"
			Poke uInteger 23675, @u (1)
			gosub checkW
		end if
	end if

	if (sq7 = 0 and ((cX = 7 and (cY = 1 or cY = 2 or cY = 3)) or _
		(cX = 6 and (cY = 1 or cY = 3)) or _
		(cX = 5 and (cY = 1 or cY = 2 or cY = 3)))) then
		if sG(7,1) > 1 and sG(7,2) > 1 and sG(7,3) > 1 and _ 
			sG(6,1) > 1 and sG(6,3) > 1 and _ 
			sG(5,1) > 1 and sG(5,2) > 1 and sG(5,3) > 1 then
			let sq7 = 1
			print at 11,11;"\{i2}\O\P"; at 12,11;"\{i2}\Q\R"
			gosub checkW
		end if
	end if
	
	if (sq8 = 0 and ((cX = 8 and (cY = 3 or cY = 4 or cY = 5)) or _
		(cX = 7 and (cY = 3 or cY = 5)) or _
		(cX = 6 and (cY = 3 or cY = 4 or cY = 5)))) then
		if sG(8,3) > 1 and sG(8,4) > 1 and sG(8,5) > 1 and _ 
			sG(7,3) > 1 and sG(7,5) > 1 and _ 
			sG(6,3) > 1 and sG(6,4) > 1 and sG(6,5) > 1 then
			let sq8 = 1
			Poke uInteger 23675, @u2 (1)
			print at 13,15;"\{i6}\I\J"; at 14,15;"\{i6}\K\L"
			Poke uInteger 23675, @u (1)
			gosub checkW
		end if
	end if

	if (sq9 = 0 and ((cX = 7 and (cY = 5 or cY = 6 or cY = 7)) or _
		(cX = 6 and (cY = 5 or cY = 7)) or _
		(cX = 5 and (cY = 5 or cY = 6 or cY = 7)))) then
		if sG(7,5) > 1 and sG(7,6) > 1 and sG(7,7) > 1 and _ 
			sG(6,5) > 1 and sG(6,7) > 1 and _ 
			sG(5,5) > 1 and sG(5,6) > 1 and sG(5,7) > 1 then
			let sq9 = 1
			Poke uInteger 23675, @u2 (1)
			print at 11,19;"\{i2}\A\B"; at 12,19;"\{i2}\C\D"
			Poke uInteger 23675, @u (1)
			gosub checkW
		end if
	end if

	if (sq10 = 0 and ((cX = 9 and (cY = 1 or cY = 2 or cY = 3)) or _
		(cX = 8 and (cY = 1 or cY = 3)) or _
		(cX = 7 and (cY = 1 or cY = 2 or cY = 3)))) then
		if sG(9,1) > 1 and sG(9,2) > 1 and sG(9,3) > 1 and _ 
			sG(8,1) > 1 and sG(8,3) > 1 and _ 
			sG(7,1) > 1 and sG(7,2) > 1 and sG(7,3) > 1 then
			let sq10 = 1
			Poke uInteger 23675, @u2 (1)
			print at 15,11;"\{i4}\M\N"; at 16,11;"\{i4}\O\P"
			Poke uInteger 23675, @u (1)
			gosub checkW
		end if
	end if
	
	if (sq11 = 0 and ((cX = 9 and (cY = 5 or cY = 6 or cY = 7)) or _
		(cX = 8 and (cY = 5 or cY = 7)) or _
		(cX = 7 and (cY = 5 or cY = 6 or cY = 7)))) then
		if sG(9,5) > 1 and sG(9,6) > 1 and sG(9,7) > 1 and _ 
			sG(8,5) > 1 and sG(8,7) > 1 and _ 
			sG(7,5) > 1 and sG(7,6) > 1 and sG(7,7) > 1 then
			let sq11 = 1
			Poke uInteger 23675, @u2 (1)
			print at 15,19;"\{i5}\Q\R"; at 16,19;"\{i5}\S\T"
			Poke uInteger 23675, @u (1)
			gosub checkW
		end if
	end if

	if (sq12 = 0 and ((cX = 11 and (cY = 1 or cY = 2 or cY = 3)) or _
		(cX = 10 and (cY = 1 or cY = 3)) or _
		(cX = 9 and (cY = 1 or cY = 2 or cY = 3)))) then
		if sG(11,1) > 1 and sG(11,2) > 1 and sG(11,3) > 1 and _ 
			sG(10,1) > 1 and sG(10,3) > 1 and _ 
			sG(9,1) > 1 and sG(9,2) > 1 and sG(9,3) > 1 then
			let sq12 = 1
			Poke uInteger 23675, @u2 (1)
			print at 19,11;"\{i6}\I\J"; at 20,11;"\{i6}\K\L"
			Poke uInteger 23675, @u (1)
			gosub checkEat
			gosub checkW
		end if
	end if

	if (sq13 = 0 and ((cX = 11 and (cY = 3 or cY = 4 or cY = 5)) or _
		(cX = 10 and (cY = 3 or cY = 5)) or _
		(cX = 9 and (cY = 3 or cY = 5)) or _
		(cX = 8 and (cY = 3 or cY = 4 or cY = 5)))) then
		if sG(11,3) = 2 and sG(11,4) > 1 and sG(11,5) > 1 and _ 
			sG(10,3) > 1 and sG(10,5) > 1 and _ 
			sG(9,3) > 1 and sG(9,5) = 2 and _
			sG(8,3) > 1 and sG(8,4) > 1 and sG(8,5) > 1 then
			let sq13 = 1
			Poke uInteger 23675, @u2 (1)
			print at 17,15;"\{i2}\A\B"; at 18,15;"\{i2}\C\D"
			Poke uInteger 23675, @u (1)
			print at 19,15;"\{i2}\O\P"; at 20,15;"\{i2}\Q\R"
			gosub checkW
		end if
	end if

	if (sq14 = 0 and ((cX = 11 and (cY = 5 or cY = 6 or cY = 7)) or _
		(cX = 10 and (cY = 5 or cY = 37)) or _
		(cX = 9 and (cY = 5 or cY = 6 or cY = 7)))) then
		if sG(11,5) > 1 and sG(11,6) > 1 and sG(11,7) > 1 and _ 
			sG(10,5) > 1 and sG(10,7) > 1 and _ 
			sG(9,5) > 1 and sG(9,6) > 1 and sG(9,7) > 1 then
			let sq14 = 1
			Poke uInteger 23675, @u2 (1)
			print at 19,19;"\{i6}\E\F"; at 20,19;"\{i6}\G\H"
			Poke uInteger 23675, @u (1)
			gosub checkEat
			gosub checkW
		end if
	end if

	let tMI = (65536* peek 23674+256*peek 23673 + peek 23672)/10

#endif

	return

checkEat:
	if sq14 = 1 and sq12 = 1 and sq3 = 1 and sq1 = 1 then
		let eat = 1	: let teat = (65536* peek 23674+256*peek 23673 + peek 23672)/10
		gosub deleteEne
		gosub checkEne
	end if

return

eatEne:

	BEEP 0.02,-35
	BEEP 0.02,-25
	BEEP 0.02,-15
	BEEP 0.02,-5
	BEEP 0.02,5
	BEEP 0.02,15
	BEEP 0.02,25
	BEEP 0.02,15
	BEEP 0.02,5
	BEEP 0.02,-5
	BEEP 0.02,-15
	BEEP 0.02,-25
	BEEP 0.02,-35

	if e1X = cX and e1Y = cY then
		let e1X = 1: let e1Y = 1 : let e1H = 1: let e1V = 1: let e1C = 3: let e1E = 1 : let sc = sc + 20
		gosub printSc
	end if	

	if e2X = cX and e2Y = cY then
		let e2X = 1: let e2Y = 3 : let e2H =-1: let e2V = 1: let e2C = 3: let e2E = 1 : let sc = sc + 20
		gosub printSc
	end if	

	if e3X = cX and e3Y = cY then
		let e3X = 1: let e3Y = 5 : let e3H = 1: let e3V = 1: let e3C = 3: let e3E = 1 : let sc = sc + 20
		gosub printSc
	end if
return

checkW:

	BEEP 0.02,5
	BEEP 0.04,-5
	BEEP 0.03,35

	let sc = sc + 20
	gosub printSc

	if sq14 = 1 and sq13 = 1 and sq12 = 1 and sq11 = 1 and sq10 = 1 and sq9 = 1 and _
	 sq8 = 1 and sq7 = 1 and sq6 = 1 and sq5 = 1 and sq4 = 1 and sq3 = 1 and sq2 = 1 and sq1 = 1 then
		gosub resetScreen

		pause 20

		cls
		gosub printMarker
		gosub printScTitle
		gosub printSc
		gosub printHighScore
		for x=1 TO 11
			for y=1 TO 7
				if sG(x,y) > 0 then 
					print at x+(x-1),(y*2)+7;c$
				end if
			next y
		next x

		let pB = Int(Rnd * 4) + 1
		if pB = 2 then 
			let pB = 3 
		else if pB = 3 then 
			let pB = 5
		else if pB = 4 then 
			let pB = 7 
		end if

		print at 11+(11-1),(pB*2)+7;c1$
		print at 11+(11-1)+1,(pB*2)+7;c2$
		
		let pA = 1 : let pAY = 1 : let pC = 0
		bucleBonus:
			beep 0.02,20
			beep 0.01,15
			beep 0.02,20
			for y=1 TO 7
				print at pAY+(pAY-1),(y*2)+7;c$; at pAY+(pAY-1)+1,(y*2)+7;"  ";
			next y
			print at pAY+(pAY-1),(pA*2)+7;c1$; at pAY+(pAY-1)+1,(pA*2)+7;c2$;
			IF In 32766 = 254 Or In 32766 = 190 OR In 32766 = 251 Or In 32766 = 187 then goto moverBonusInit

			pause 2
			let pA = pA + 2
			if pA > 7 then let pA = 1

		goto bucleBonus

		moverBonusInit:
			for y=1 TO 2
				gosub alternarCerdo
				print at pAY+(pAY-1),(pA*2)+7;p$; at pAY+(pAY-1)+1,(pA*2)+7;"  ";
				let pAY = pAY + 1
				beep 0.02,40
				beep 0.01,35
				beep 0.02,40
				print at pAY+(pAY-1),(pA*2)+7;c1$; at pAY+(pAY-1)+1,(pA*2)+7;c2$;
				pause 7
			next

			print at pAY+(pAY-1),(pA*2)+7;p$; at pAY+(pAY-1)+1,(pA*2)+7;"  ";

			checkSides:
			gosub alternarCerdo
			if pA < 7 and sG(pAY,pA+1) > 0 then
				let pA = pA + 1 : let pH = 1 : let pC = pC + 1
			else if pA > 1 and sG(pAY,pA-1) > 0 then
				let pA = pA - 1 : let pH = -1 : let pC = pC + 1
			else
				let pAY = pAY + 1
				beep 0.02,40
				beep 0.01,35
				beep 0.02,40
				print at pAY+(pAY-1),(pA*2)+7;c1$; at pAY+(pAY-1)+1,(pA*2)+7;c2$;
				pause 7

				print at pAY+(pAY-1),(pA*2)+7;p$; at pAY+(pAY-1)+1,(pA*2)+7;"  ";
				goto checkSides
			end if

		moverBonus:		
			gosub alternarCerdo
			print at pAY+(pAY-1),(pA*2)+7;p$; at pAY+(pAY-1)+1,(pA*2)+7;"  ";
		
			if pA < 7 and pH = 1 and sG(pAY,pA+1) > 0 and pC <= 2 then
				let pA = pA + 1 : let pC = pC + 1
			else if pA > 1 and pH = -1 and sG(pAY,pA-1) > 0 and pC <= 2 then
				let pA = pA - 1 : let pC = pC + 1
			else			
				if pA >= 6 then let pH = -1
				if pA <= 1 then let pH = 1
				let pAY = pAY + 1 : let pC = 0
			end if

			beep 0.02,40
			beep 0.01,35
			beep 0.02,40
			print at pAY+(pAY-1),(pA*2)+7;c1$; at pAY+(pAY-1)+1,(pA*2)+7;c2$;
			pause 7

			if pAY <> 11 then goto moverBonus
		pause 7


		if pB = pA then
			FOR y=15 TO 1 STEP -1
				BEEP .0025,20-y
				BEEP .0025,20-y-1
			NEXT y
			BEEP .05,-10
			let sc = sc + 10
			gosub printSc
		else
			BEEP .05,-50			
		end if

		for y=1 TO 7
			print at pAY+(pAY-1),(pA*2)+7;p$; at pAY+(pAY-1)+1,(pA*2)+7;"  ";

			pause 5
				beep 0.02,40
				beep 0.01,35
				beep 0.02,40
			print at pAY+(pAY-1),(pA*2)+7;c1$; at pAY+(pAY-1)+1,(pA*2)+7;c2$;
			pause 5
		next

		let sG(11,4) = 2
		goto showScreen

	end if

return

colision:
	for y=1 TO 7		
		print at cX+(cX-1),(cY*2)+7;e1$; at cX+(cX-1)+1,(cY*2)+7;e2$;
		pause 15
		
		gosub printCerdo

		BEEP 0.01,35
		BEEP 0.01,25
		BEEP 0.01,15
		BEEP 0.01,-5

		pause 15
	next y

	#ifdef __CARGA_B
	gosub resetRect
	#endif

	GOSUB deletePig:
	GOSUB deleteEne:
	
	let er = 0 : let fr = 0 : let life = life - 1

	gosub printMarker
	
	if life < 1 then

		print at 9,10;"\{i0}           "; at 10,10;"\{i0}           "; at 11,10;"\{i0}           ";
  		LET go$="GAME OVER"
  		FOR z=1 TO LEN go$
  		BEEP 0.005,z
  		PRINT at 10,10+z;go$(z);: PAUSE 15
  		NEXT z

  		FOR y=30 TO 1 step -1
  			BEEP 0.005,y
  		NEXT y

		if sc > hSC then 
			let hSC = sc : LET nhi$="NEW HI-SCORE"
			print at 13,9;"\{i0}                "; at 14,9;"\{i0}                "; at 15,9;"\{i0}                ";
	  		FOR z=1 TO LEN nhi$
  			BEEP 0.005,z*2
	  		PRINT at 14,9+z;flash 1;nhi$(z);: PAUSE 10
	  		NEXT z
			FOR z=1 TO 7
  			BEEP 0.005,z*3
	  		PRINT at 14,10; ink z;nhi$;: PAUSE 5
	  		NEXT z
	  		PRINT at 14,10; flash 1; ink 7;nhi$;
		end if

		gosub resetScreen
		let sc = 0 : let sG(11,4) = 2

		pause 0
		let life = 3 : let jC = 3 : LET pE = 10 : LET jE = 9
		go sub initLoad
	end if
	
	gosub printMarker
	gosub resetEne
	gosub printCerdo	
	gosub altE
	gosub printE

return 

resetEne:
	
	let cX = 11
	let cY = 4

	#ifdef __CARGA_B
	let clX = 11 : let clLX = 11
	let clY = 4 : let clLY = 4
	let sSq = 13
 	#endif

	let e1X = 1: let e1Y = 1 : let e1H = 1: let e1V = 1: let e1C = 3: let e1E = 0 : _
	let e2X = 1: let e2Y = 3 : let e2H =-1: let e2V = 1: let e2C = 3: let e2E = 0 : _
	let e3X = 1: let e3Y = 5 : let e3H = 1: let e3V = 1: let e3C = 3: let e3E = 0

	gosub altE

return

resetScreen:
	for x=1 TO 11
		for y=1 TO 7
			if sG(x,y) > 1 then let sG(x,y) = 1
		next y
	next x
	let eat = 0
return

printMarker:
	print at 1,6; jC; at 3,1; "      "; at 3+1,1; "      ";
	if life > 0 then print at 3,1;c1$; at 3+1,1;c2$;
	if life > 1 then print at 3,3;c1$; at 3+1,3;c2$;
	if life > 2 then print at 3,5;c1$; at 3+1,5;c2$;
	gosub printCerdoMarker
return

printCerdoMarker:
	Poke uInteger 23675, @uM (1)
	print at 18,27;"\{i3}\A\B\C"; at 19,27;"\{i3}\D\E\F"; at 20,27;"\{i3}\G\H\I"; at 21,27;"\{i3}\J\K\L"; at 8,3;"\{i5}\M\N"; at 9,3;"\{i5}\O\P"; at 11,3;"\{i5}\Q\R"; at 12,3;"\{i5}\S\T";

	Poke uInteger 23675, @u2 (1)	
	print at 20,3;"\{i2}\A\B"; at 21,3;"\{i2}\C\D";
	Poke uInteger 23675, @u (1)
	print at 8,28;"\{i2}\O\P"; at 9,28;"\{i2}\Q\R";

	print at 14,3;"\{i4}\A\B"; at 16,3;"\{i4}\A\B"; at 18,3;"\{i4}\A\B"; at 21,6;"\{i4}\A\B"; at 21,24;"\{i4}\A\B";

return

printScTitle:
	print at 1,1; "JUMP:"; at 4,27; "SCORE"; at 1,24; "HI-SCORE";	
return

printSc:
	if sc < 10 then 
		print at 5,28; "000";sc
	else if sc < 100 then 
		print at 5,28; "00";sc
	else if sc < 1000 then 
		print at 5,28; "0";sc
	else if sc > 9999 then 
		print at 5,27; sc
	else
	print at 5,28; sc
	end if
return

printHighScore:
	if hSC < 10 then 
		print at 2,28; "000";hSC
	else if hSC < 100 then 
		print at 2,28; "00";hSC
	else if hSC < 1000 then 
		print at 2,28; "0";hSC
	else if hSC > 9999 then 
		print at 2,27; hSC
	else
	print at 2,28; hSC
	end if
return

alternarCerdo:	
	let fr=1-fr
	if fr=0 then
		LET c1$= "\{i3}\K\L"
	Else
		LET c1$= "\{i3}\S\T"
	end If
return

altE:	
	if er=0 and eat = 1 then
		LET e1$ = "\{f1}\C\D" : LET e2$ = "\{f1}\E\F"
	Else if eat = 1 then
		LET e1$ = "\{f1}\G\H" : LET e2$ = "\{f1}\I\J"
	else if er=0 then
		LET e1$ = "\{i5}\C\D" : LET e2$ = "\{i5}\E\F"
	Else
		LET e1$ = "\{i5}\G\H" : LET e2$ = "\{i5}\I\J"
	end If
return

printE:
	if e1E = 0 then 
	print at e1X+(e1X-1),(e1Y*2)+7;e1$; at e1X+(e1X-1)+1,(e1Y*2)+7;e2$;
	end if
	
	if e2E = 0 then
	print at e2X+(e2X-1),(e2Y*2)+7;e1$; at e2X+(e2X-1)+1,(e2Y*2)+7;e2$;
	end if

	if e3E = 0 then
	print at e3X+(e3X-1),(e3Y*2)+7;e1$; at e3X+(e3X-1)+1,(e3Y*2)+7;e2$;
	end if
return

printCerdo:
	print at cX+(cX-1),(cY*2)+7;c1$; at cX+(cX-1)+1,(cY*2)+7;c2$;
return
